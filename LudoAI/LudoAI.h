#pragma once
#include "../Ludo-help-for-tools-of-AI/Domain-Code/iplayer.h"
#include "../Ludo-help-for-tools-of-AI/Domain-Code/positions_and_dice.h"
#include "io_vector.h"
#include "ANN.h"

#include <iostream>
#include <algorithm>

#define NPIECES 16
#define PLAYERPIECES 4


class LudoAI :
	public iplayer
{
public:
	void printState();
	LudoAI();
	LudoAI(ANN neuralNet);
	std::vector<double> encodeGenome();
	void decodeGenome(const std::vector<double>& gene);

	iplayer* clone() const { return new LudoAI(*this); }

	ANN decisionMaker;

private:
	int make_decision();
	void update_state();
	int kill_or_die(int pos);
	

	std::vector<double> construct_state_vector();

	std::vector<bool> inStart;
	std::vector<bool>  inGoal;
	std::vector<bool>  isSafe; //on globe or two+ pieces together or goal stretch
	std::vector<bool>  canBeKilled; //in reach of enemy player
	std::vector<bool>  canKill; // can send enemy home with current dice roll
	std::vector<bool>  beSafe; // can get to safety (globe, teammate, goal stretch)
	std::vector<bool>  canJump; // can reach star with current dice roll
	std::vector<bool>  canFinish; // can reach goal pos with current dice roll
	std::vector<bool>  willDie; // will be sent home with current dice roll
	std::vector<bool> isValid; // is able to move
	bool  isSix;
	std::vector<double>  progress;

	
	
	
	


};

