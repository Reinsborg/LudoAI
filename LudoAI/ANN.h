#pragma once
#include <vector>
#include <functional>
#include <algorithm>

class ANN
{
public:
	ANN();
	ANN(size_t nInputs, size_t nOutputs, size_t nLayers, size_t nNodes);
	void setActivation(void (*actFunc)(const std::vector<double>&, std::vector<double>&));
	void setActivation(void (*actFunc)(const std::vector<double>&, std::vector<double>&), void (*outFunc)(const std::vector<double>&, std::vector<double>& ));
	void setActivation(std::vector< void (*)(const std::vector<double>&, std::vector<double>&) > actFunctions);
	void initialize(std::function<double()>& initializer);

	std::vector<double> forward(const std::vector<double>& input);
	std::vector< std::vector< std::vector< double >>>& getWeights();
	std::vector< std::vector< double >>& getBiases();
	void setWieghts(std::vector< std::vector< std::vector< double >>>& weigths);
	void setBiases(std::vector< std::vector< double >>&  biases);

	std::vector<double> encodeGenome();
	void decodeGenome(const std::vector<double>& gene);

private:
	std::vector<size_t> inputDim;
	std::vector< void (*)(const std::vector<double>& , std::vector<double>&) > activation;
	std::vector< std::vector< std::vector< double >>> weights; // layer > node > weights
	std::vector< std::vector< double >> biases; // layer > nodeBias

};

void RELU(const std::vector<double>& activity, std::vector<double>& output);


void sigmoid(const std::vector<double>& activity, std::vector<double>& output);

void tanh_vec(const std::vector<double>& activity, std::vector<double>& output);

void softMax(const std::vector<double>& activity, std::vector<double>& output);

//tanh: return tanh(x)
