#include "player_expert.h"


player_expert::player_expert() : inStart(NPIECES, true), inGoal(NPIECES, false), isSafe(NPIECES, true), canBeKilled(PLAYERPIECES, false),
canKill(PLAYERPIECES, false), beSafe(PLAYERPIECES, false), canJump(PLAYERPIECES, false),
canFinish(PLAYERPIECES, false), willDie(PLAYERPIECES, false), progress(NPIECES, 0.0), isValid(PLAYERPIECES, false)
{

}


void player_expert::update_state()
{
	unsigned int color;
	unsigned int pos;
	isSix = dice == 6;

	// update states relavant to all pices: inStart, inGoal, isSafe
	// not all are inputs but helpers to calculate actual inputs
	for (size_t piece = 0; piece < NPIECES; piece++)
	{
		inStart[piece] = position[piece] < 0;
		inGoal[piece] = position[piece] == 99;
		if (piece < 4)
			isSafe[piece] = position[piece] % 13 == 0 || position[piece] % 13 == 8 || position[piece] >= 51 || position[piece] < 0;
		else
			isSafe[piece] = (position[piece] % 13 == 0 && position[piece] != 0) || position[piece] % 13 == 8 || position[piece] >= 51 || position[piece] < 0;
		color = piece / 4;
		if (!isSafe[piece]) // check if piece is protected by teammate
		{

			for (size_t teammate = color * 4; teammate < color * 4 + 4; teammate++)
			{
				if (teammate == piece)
					continue;
				else
				{
					if (position[teammate] == position[piece] && position[piece] != 0)
					{
						isSafe[piece] = true;
						//isSafe[teammate] = true;
					}
				}


			}
		}

		//calculate progress of each piece
		if (inStart[piece])
		{
			progress[piece] = 0;
		}
		else if (inGoal[piece])
		{
			progress[piece] = 1;
		}
		else
		{
			if (position[piece] > 55) // goal stretch is special case
			{
				pos = position[piece] - (5 * color);
			}
			else
			{
				pos = position[piece] - (13 * color);
				if (pos < 0)
				{
					pos += 52;
				}
			}
			progress[piece] = double(pos) / 56.0;
		}

	}

	// update states only applicable to friendly pieces ********************************************************************
	for (size_t friendly = 0; friendly < 4; friendly++)
	{
		canBeKilled[friendly] = false;
		canKill[friendly] = false;
		canFinish[friendly] = false;
		canJump[friendly] = false;
		beSafe[friendly] = false;
		willDie[friendly] = false;




		// if not safe, check if piece is in reach of enemy ****************************************************************
		if (!isSafe[friendly])
		{
			for (size_t enemy = 4; enemy < NPIECES; enemy++)
			{
				int dist = position[friendly] - position[enemy];
				if ((dist > 0 && dist <= 6) || (dist <= -46 && dist >= -51))
				{
					canBeKilled[friendly] = true;
				}
			}
		}
		//******************************************************************************************************************

		if (!is_valid_move(friendly))
		{
			isValid[friendly] = false;
		}
		else
		{
			isValid[friendly] = true;
			int new_pos;

			if (inStart[friendly])
			{
				new_pos = 0;
			}
			else
			{
				new_pos = position[friendly] + dice; // intermediate position of the piece after move
			}

			if (new_pos == 56) // check if goal, no furter action needed
			{
				canFinish[friendly] = true;
				beSafe[friendly] = true;
			}
			else
			{


				// check if it will die or kill at intermediate pos ****************************************************************
				int kill_die = kill_or_die(new_pos);
				if (kill_die == -1) // if the piece die at the intermediate pos no further investegation is needed
				{
					willDie[friendly] = true;
				}
				else
				{
					if (kill_die == 1)
					{
						canKill[friendly] = true;
					}

					// check if intermediate pos is a star and update arcordingly ***************************************************
					if (new_pos % 13 == 5 || new_pos == 50)
					{
						canJump[friendly] = true;
						new_pos += 6;
					}
					else if (new_pos % 13 == 11)
					{
						canJump[friendly] = true;
						new_pos += 7;
					}
					else
					{
						canJump[friendly] = false;
					}
					//***************************************************************************************************************


					if (new_pos == 56) // check if new position is the goal, no further action needed
					{
						canFinish[friendly] = true;
						beSafe[friendly] = true;
					}
					else
					{
						if (canJump[friendly]) // if we jumped recheck death/kill
						{
							int tmp = kill_or_die(new_pos);
							if (tmp == -1) // if the piece die at the intermediate pos no further investegation is needed
							{
								willDie[friendly] = true;
							}
							if (tmp == 1)
							{
								canKill[friendly] = true;
							}
						}
						beSafe[friendly] = new_pos % 13 == 8 || new_pos > 50; // land on globe or goal stretch
						if (!beSafe[friendly])
						{
							for (size_t teammate = 0; teammate < 4; teammate++) // land on teammate
							{
								if (teammate != friendly && position[teammate] == new_pos)
								{
									beSafe[friendly] = true;
								}
							}
						}
					}
				}
			}
		}
	}
}

int player_expert::kill_or_die(int pos)
{
	for (size_t enemy = 4; enemy < NPIECES; enemy++)
	{

		if (position[enemy] == pos)
		{
			if (isSafe[enemy])
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}

	}
	return 0;
}

int player_expert::make_decision()
{
	update_state();
	std::vector<int> posible_moves;
	for (size_t i = 0; i < PLAYERPIECES; i++)
	{
		if (is_valid_move(i) && !willDie[i])
		{
			posible_moves.push_back(i);
		}
	}

	if (posible_moves.empty())
	{
		return 0;
	}

	if (isSix)
	{
		for (size_t i = 0; i < PLAYERPIECES; i++)
		{
			if (inStart[i])
			{
				return i;
			}
		}
	}
	for (auto& piece : posible_moves)
	{
		if (canKill[piece])
		{
			return piece;
		}
	}

	for (auto& piece : posible_moves)
	{
		if (canBeKilled[piece])
		{
			return piece;
		}
	}

	for (auto& piece : posible_moves)
	{
		if (canFinish[piece])
		{
			return piece;
		}
	}

	for (auto& piece : posible_moves)
	{
		if (beSafe[piece])
		{
			return piece;
		}

	}

	for (auto& piece : posible_moves)
	{
		if (!isSafe[piece])
		{
			return piece;
		}
	}

	return posible_moves[0];
}