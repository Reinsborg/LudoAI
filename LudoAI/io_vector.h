#pragma once

#include <iostream>
#include <vector>

template <class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& input)
{
	
	for (typename std::vector<T>::const_iterator ii = input.begin(); ii != input.end(); ++ii)
	{
		os << *ii << ", ";
	}
	

	return os;
}

