#pragma once
#include <vector>
#include <functional>
#include <algorithm>
#include <future>
#include <iostream>
#include <numeric>
#include <string>
#include <fstream>

#include "LudoAI.h"
#include "../Ludo-help-for-tools-of-AI/Domain-Code/game.h"
#include "../Ludo-help-for-tools-of-AI/Domain-Code/iplayer.h"
#include "../Ludo-help-for-tools-of-AI/Domain-Code/game.h"
#include "io_vector.h"


template <typename DNA> class GeneticAlgorithm
{
public:
	GeneticAlgorithm(LudoAI* defaultAgent, iplayer* simple_agent, iplayer* adv_agent, std::mt19937 & generator, std::function<void(DNA&)>& mutator, double mutateRate = 0.01, int nPointC = 1);
	~GeneticAlgorithm() { geneSaver.close(); dataSaver.close(); }

    double evolve(size_t nWorkers, double targetFitness, size_t nMatchUpsPerSpeciment = 30, size_t nSamplesPerMatch = 8, double elitism = 0.10, size_t max_generations = 0, size_t evaluate = 0, bool fitByMatchUp = false, bool selectByProb= false, double tournamentSize = 0.25);


	std::vector<double>  evaluateSpeciment(const std::vector<DNA>& gene, size_t N);


	void initialPopulation(std::vector<std::vector<DNA>> pop);

	void unit_test();
	
	std::string geneRecord;
	std::string performanceFile;
	std::string apexFile;
	

private:
	int nPointCrossover;
	double mutationRate;
	std::vector<std::vector<DNA>> crossover(const std::vector<DNA>& parent_A, const std::vector<DNA>& parent_B);
	void mutate(std::vector<DNA>& gene);
    void mutate_v2(std::vector<DNA>& gene);
	std::vector<size_t[2]> selectParents(const std::vector<double>& balot, size_t totalWins, size_t Ncouples);
	std::vector<size_t[2]> selectParents_v2(const std::vector<double>& wins, size_t Ncouples, size_t compare_sample_size);


	std::vector<double> estimateFitness_byRandomMatchUp(size_t nMatchUpsPerSpeciment, size_t nSamplesPerMatch);
	std::vector<double> estimateFitness_bySimpleWinrate(size_t specimen_id, size_t samples);

	std::vector<std::vector<DNA>> population;
	std::function<void(DNA&)> mutator;

	std::mt19937 generator;
	std::uniform_int_distribution<int> distribution;

	LudoAI* base_agent;
	iplayer* base_agent_simple;
	iplayer* base_agent_adv;

	std::ofstream geneSaver;
	std::ofstream dataSaver;

	std::vector<std::pair<int, int>> IDs;
};

template <typename DNA>
std::vector<double> GeneticAlgorithm<DNA>::estimateFitness_byRandomMatchUp(size_t nMatchUpsPerSpeciment, size_t nSamplesPerMatch)
{
	std::uniform_int_distribution<size_t> sampleDist;
	std::vector<double> res(population.size()+2, 0);
	std::vector<size_t> bagOfFate;
	size_t index_A;
	size_t index_B;
	size_t contestant_A;
	size_t contestant_B;

	LudoAI* playerA = new LudoAI(*base_agent);
	LudoAI* playerB = new LudoAI(*base_agent);
	iplayer* playerC =  base_agent_simple->clone();
	iplayer* playerD = base_agent_adv->clone();

	bagOfFate.reserve(population.size() * nMatchUpsPerSpeciment);
	for (size_t i = 0; i < population.size(); i++)
	{
		bagOfFate.insert(bagOfFate.end(), nMatchUpsPerSpeciment, i);
	}
	if (bagOfFate.size() % 2 != 0)
		throw "population size times matchUpPerSpecimen must be even number";

	while (!bagOfFate.empty())
	{
        sampleDist.param(std::uniform_int_distribution<size_t>::param_type(0, bagOfFate.size() - 1));
		index_A = sampleDist(generator);
		contestant_A = bagOfFate.at(index_A);
		bagOfFate.erase(bagOfFate.begin() + index_A);

        sampleDist.param(std::uniform_int_distribution<size_t>::param_type(0, bagOfFate.size() - 1));
		index_B = sampleDist(generator);
		contestant_B = bagOfFate.at(index_B);
		bagOfFate.erase(bagOfFate.begin() + index_B);

		playerA->decodeGenome(population.at(contestant_A));
		playerB->decodeGenome(population.at(contestant_B));

		game ludo(playerA, playerB, playerC, playerD, generator);

		// Play many games of Ludo
		int wins[] = { 0, 0, 0, 0 };
		for (int i = 0; i < nSamplesPerMatch; i++)
		{
			ludo.reset();
			ludo.set_first(i % 4); //alternate who starts the game
			ludo.play_game();
			wins[ludo.get_winner()]++;
		}
		res.at(contestant_A) += wins[0];
		res.at(contestant_B) += wins[1];
		*(res.end() - 2) += wins[2];
		*(res.end() - 1) += wins[3];
	}
	//double totalGamesPerSpeciment = nMatchUpsPerSpeciment * nSamplesPerMatch;
	//std::for_each(res.begin(), res.end(), [totalGamesPerSpeciment](double& winrate) {winrate = winrate / totalGamesPerSpeciment; });


	delete playerA;
	delete playerB;
	delete playerC;
	delete playerD;


	return res;
}

template<typename DNA>
inline std::vector<double> GeneticAlgorithm<DNA>::estimateFitness_bySimpleWinrate(size_t specimen_id, size_t samples)
{
	LudoAI* playerA = new LudoAI(*base_agent);
	iplayer* playerB = base_agent_simple->clone();
	iplayer* playerC = base_agent_simple->clone();
	iplayer* playerD = base_agent_simple->clone();

	std::vector<double> res(population.size() + 2, 0);

	playerA->decodeGenome(population.at(specimen_id));

	game ludo(playerA, playerB, playerC, playerD, generator);

	// Play many games of Ludo
	int wins[] = { 0, 0, 0, 0 };
	for (int i = 0; i < samples; i++)
	{
		ludo.reset();
		ludo.set_first(i % 4); //alternate who starts the game
		ludo.play_game();
		wins[ludo.get_winner()]++;
	}

	res.at(specimen_id) = wins[0];
	res.back() = wins[1] + wins[2] + wins[3];


	delete playerA;
	delete playerB;
	delete playerC;
	delete playerD;

	return res;
}

template <typename DNA>
void GeneticAlgorithm<DNA>::initialPopulation(std::vector<std::vector<DNA>> pop)
{
	population = pop;
}

template<typename DNA>
inline void GeneticAlgorithm<DNA>::unit_test()
{

	std::vector<double> gene1;
	std::vector<double> gene0;
    std::vector<std::vector<double>> gener;

	for (size_t i = 0; i < 10; i++)
	{
		gene1.push_back(i+1);
		gene0.push_back(10-i);
	}
	
	std::cout << "crossover test: " << std::endl;
	std::cout << gene1 << gene0 << std::endl;
	std::cout << crossover(gene1, gene0) << std::endl;

	std::cout << std::endl << "Mutate test:" << std::endl;
    gener.push_back(gene0);
    gener.push_back(gene1);
	for (size_t i = 0; i < 100; i++)
	{
        std::for_each(gener.begin(), gener.end(), std::bind(&GeneticAlgorithm<DNA>::mutate_v2, this, std::placeholders::_1));
        std::cout << gener.at(0) << std::endl;
	}

	std::vector<std::vector<DNA>> genes;
	genes.push_back( population.at(0) );
	std::vector<LudoAI*> players;
	for (size_t i = 0; i < 10; i++)
	{
		players.push_back(new LudoAI(*base_agent));
	} 
	for (size_t i = 0; i < players.size(); i++)
	{
		players.at(i)->decodeGenome(genes.at(i));
		genes.push_back(players.at(i)->encodeGenome());
	}
	for (size_t i = 0; i < players.size(); i++)
	{
		if (genes.at(0) != genes.at(i))
		{
			std::cout << "genes are not equal after encoding/decoding" << std::endl;
		}
		if (players.at(0)->decisionMaker.getWeights() != players.at(i)->decisionMaker.getWeights())
		{
			std::cout << "ANN weight change arter encdode/decode" << std::endl;
		}
		if (players.at(0)->decisionMaker.getBiases() != players.at(i)->decisionMaker.getBiases())
		{
			std::cout << "ANN biases change afyer encdode/decode"  << std::endl;
		}
	}

}


template < typename DNA>
double GeneticAlgorithm<DNA>::evolve(size_t nWorkers, double targetFitness, size_t nMatchUpsPerSpeciment, size_t nSamplesPerMatch, double elitism, size_t max_iterations, size_t evaluate, bool fitByMatchUp, bool selectByProb, double tournamentSize)
{
	if (population.size() < 4)
	{
		throw "Initial population is too small";
	}

	geneSaver.open(geneRecord, std::ios::out | std::ios::app);
	dataSaver.open(performanceFile, std::ios::out | std::ios::app);
	dataSaver << "Generation, Max_fitness, " << std::endl;

	double maxFitness = 0.0;
	size_t generation = 0;

	std::vector<std::future<std::vector<double>>> workers;
	std::vector<double> wins(population.size());
	int totalWins;
	std::vector<size_t[2]> parents;
	std::vector<std::vector<DNA>> children;
	children.reserve(population.size());
	std::vector<std::vector<DNA>> offspring;
	double simpleWins;
	double advWins;
	std::vector<DNA> apex_predator;
	double apex_fitness = 0.0;
    double nGamesPrPlayer;
    double totalGames;

    if(fitByMatchUp)
    {
        nGamesPrPlayer = nMatchUpsPerSpeciment * nSamplesPerMatch;
        totalGames = double(nMatchUpsPerSpeciment * nSamplesPerMatch * population.size()) / 2.0;
    }
    else
    {
        nGamesPrPlayer = nSamplesPerMatch;
        totalGames = nSamplesPerMatch * population.size();
    }

	for (size_t i = 0; i < population.size(); i++)
	{
        IDs.push_back(std::pair<int,int>(0, i));
	}
	int id = IDs.back().second;

	while (maxFitness < targetFitness && (max_iterations == 0 || generation < max_iterations))
	{

		std::cout << std::endl << "Generation: " << generation << std::endl;
		generation++;
		//estimate fitness values of population
        if(fitByMatchUp)
        {
            for (size_t i = 0; i < nWorkers; i++)
            {
                workers.emplace_back(std::async(std::launch::async, &GeneticAlgorithm<DNA>::estimateFitness_byRandomMatchUp, this, std::ceil(double(nMatchUpsPerSpeciment) / double(nWorkers)), nSamplesPerMatch));
            }

        }
        else
        {
            for (size_t i = 0; i < population.size(); i++)
            {
                workers.emplace_back(std::async(std::launch::async, &GeneticAlgorithm<DNA>::estimateFitness_bySimpleWinrate, this, i, nSamplesPerMatch));
            }
        }


		wins.assign(population.size(), 0);
		totalWins = 0;
		simpleWins = 0;
		advWins = 0;

		for(size_t i = 0; i < workers.size(); i++)
		{
			std::vector<double> res = workers.at(i).get();
			
			std::transform(wins.begin(), wins.end(), res.begin(), wins.begin(), std::plus<double>());
			simpleWins += *(res.end() - 2);
			advWins += *(res.end() - 1);
		}
		workers.clear();
		totalWins = std::accumulate(wins.begin(), wins.end(), 0);
        if(selectByProb)
            parents = selectParents(wins, totalWins, size_t(population.size() * (1-elitism)/2));
        else
            parents = selectParents_v2(wins, size_t(population.size() * (1 - elitism) / 2), population.size() * tournamentSize);
		
		for (size_t i = 0; i < parents.size(); i++)
		{
			offspring = crossover(population.at(parents.at(i)[0]), population.at(parents.at(i)[1]));
			std::move(offspring.begin(), offspring.end(), std::back_inserter(children));
		}
        std::for_each(children.begin(), children.end(), std::bind(&GeneticAlgorithm<DNA>::mutate_v2, this, std::placeholders::_1));
		std::vector<std::pair<int, int>> new_ids;
		for (size_t i = 0; i < children.size(); i++)
		{
            new_ids.push_back(std::pair<int,int>(generation, ++id));
		}

		//if (evaluate > 100)
		//{
		//	std::vector<double> winrates(2, 0.0);
		//	for (size_t i = 0; i < nWorkers; i++)
		//	{
		//		workers.emplace(std::async(std::launch::async, &GeneticAlgorithm::evaluateSpeciment, this, population.at(std::distance(wins.begin(), std::max_element(wins.begin(), wins.end())))));
		//	}
		//	for (size_t i = 0; i < nWorkers; i++)
		//	{
		//		std::transform(winrates.begin(), winrates.end(), workers.at(i).get().begin(), winrates.begin(), std::plus<double>());
		//	}
		//	workers.clear();
		//	std::transform(winrates.begin(), winrates.end(), winrates.begin(), [nWorkers](double x)->double {return x / nWorkers; });
		//	std::cout << "Win rates, best parent vs: " << std::endl
		//		<< "Simple agent: " << winrates.at(0) << "\tAdv. agent: " << winrates.at(1) << std::endl << std::endl;
		//}
		//else if (evaluate)
		if(evaluate)
		{
			std::vector<double> winrates = evaluateSpeciment(population.at(std::distance(wins.begin(), std::max_element(wins.begin(), wins.end()))), evaluate);
			std::cout << "Win rates vs: " << std::endl
				<< "Simple agent: " << winrates.at(0) << "\tAdv. agent: " << winrates.at(1) << std::endl;
			maxFitness = winrates.at(0);
		}
		else
		{
            maxFitness = *std::max_element(wins.begin(), wins.end()) / nGamesPrPlayer;
			std::cout << "Max fitness:\t" << maxFitness << std::endl;
			//maxFitness = (1 - double(simpleWins + advWins) / double(population.size() * nMatchUpsPerSpeciment * nSamplesPerMatch / 2));
			//std::cout << "Average winrate of population vs static agents: " << maxFitness << std::endl << std::endl;
		}
        double mean_fitness = 1.0 - double(simpleWins + advWins)/totalGames;
        double min_fitness = *std::min_element(wins.begin(), wins.end()) / nGamesPrPlayer;
        std::cout << "Mean fitness:\t" << mean_fitness << std::endl;
        std::cout << "Min fitness:\t" << min_fitness << std::endl;
        dataSaver << generation << ", "<< maxFitness << ", " << mean_fitness << ", " << min_fitness << std::endl;
		geneSaver << population.at(std::distance(wins.begin(), std::max_element(wins.begin(), wins.end()))) << std::endl;
		auto maxElement = std::max_element(wins.begin(), wins.end());
		if (*maxElement > apex_fitness)
		{
			apex_predator = population.at(std::distance(wins.begin(), maxElement));
			apex_fitness = *maxElement;
		}
		
		
		for (size_t i = children.size(); i < population.size(); i++)
		{
			auto maxElement = std::max_element(wins.begin(), wins.end());
			size_t index = std::distance(wins.begin(), maxElement);
			std::cout << "Surviving parent: " << IDs.at(index).second << " from generation: " << IDs.at(index).first << " age: " << generation - IDs.at(index).first << std::endl;
			children.push_back(population.at(index));
			new_ids.push_back(IDs.at(index));
			*maxElement = 0;
			
		}

		population = std::move(children);
		IDs = std::move(new_ids);
		//if (iteration % 100 == 0)
		//{
		//	dataSaver << std::flush;
		//	geneSaver << std::flush;
		//}

	}

	
	double victory = 0.0;
	for (size_t i = 0; i < nWorkers; i++)
	{
		workers.emplace_back(std::async(std::launch::async, &GeneticAlgorithm<DNA>::evaluateSpeciment, this, std::ref(apex_predator), 100000 / nWorkers));
	}

	for (size_t i = 0; i < workers.size(); i++)
	{
		std::vector<double> res = workers.at(i).get();
		victory += (res.at(0) + res.at(1))/2;
	}
	victory = victory / nWorkers;
	std::ofstream file;
	file.open(apexFile);
	file << apex_predator << std::endl;
	file << "Win rate = " << victory;

	std::cout << "Apex win rate = " << victory << std::endl << std::endl;
	
	return victory;

}

template< typename DNA>
inline std::vector<double> GeneticAlgorithm<DNA>::evaluateSpeciment(const std::vector<DNA>& gene, size_t N)
{
	std::vector<double> winrate;
	LudoAI* playerA = new LudoAI(*base_agent);
	iplayer* playerB = base_agent_simple->clone();
	iplayer* playerC = base_agent_simple->clone();
	iplayer* playerD = base_agent_simple->clone();

	playerA->decodeGenome(gene);

	game ludo(playerA, playerB, playerC, playerD, generator);

	int wins[] = { 0, 0, 0, 0 };
	for (int i = 0; i < N; i++)
	{
		ludo.reset();
		ludo.set_first(i % 4); //alternate who starts the game
		ludo.play_game();
		wins[ludo.get_winner()]++;
	}
	winrate.push_back(double(wins[0]) / double(N));


	iplayer* playerE = base_agent_adv->clone();
	iplayer* playerF = base_agent_adv->clone();
	iplayer* playerG = base_agent_adv->clone();

	ludo.set_players(playerA, playerE, playerF, playerG);

	int wins2[] = { 0, 0, 0, 0 };
	for (int i = 0; i < N; i++)
	{
		ludo.reset();
		ludo.set_first(i % 4); //alternate who starts the game
		ludo.play_game();
		wins2[ludo.get_winner()]++;
	}
	winrate.push_back(double(wins2[0]) / double(N));

	delete playerA;
	delete playerB;
	delete playerC; 
	delete playerD;
	delete playerE;
	delete playerF;
	delete playerG;

	return winrate;
}



template < typename DNA>
 GeneticAlgorithm<DNA>::GeneticAlgorithm(LudoAI* default_agent, iplayer* simple_agent, iplayer* adv_agent, std::mt19937& generator, 
	 std::function<void(DNA&)>& muta, double mutateRate, int nPointC)
	: mutationRate(mutateRate), nPointCrossover(nPointC), mutator(muta), generator(generator), base_agent(default_agent), 
	  base_agent_simple(simple_agent), base_agent_adv(adv_agent), geneRecord("../../output/generations.csv"), performanceFile("../../output/data.csv")
{
}

template<typename DNA>
std::vector<std::vector<DNA>> GeneticAlgorithm<DNA>::crossover(const std::vector<DNA>& parent_A, const std::vector<DNA>& parent_B)
{
	std::vector<std::vector<DNA>> children(2, std::vector<DNA>(parent_A.size()));
	/*for (size_t i = 0; i < nPointCrossover+1; i++)
	{
		size_t crossoverLength = parent_A.size() / (nPointCrossover + 1);
		std::copy(parent_A.begin() + crossoverLength * i, parent_A.begin() + crossoverLength * (i + 1), children.at(i % 2).begin() + crossoverLength * i);
		std::copy(parent_B.begin() + crossoverLength * i, parent_B.begin() + crossoverLength * (i + 1), children.at((i + 1) % 2).begin() + crossoverLength * i);
	}*/

	size_t crossoverLength = parent_A.size() / (nPointCrossover + 1);
	for (size_t i = 0; i < parent_A.size(); i++)
	{
		if (i / crossoverLength % 2)
		{
			children.at(0).at(i) = parent_A.at(i);
			children.at(1).at(i) = parent_B.at(i);
		}
		else
		{
			children.at(0).at(i) = parent_B.at(i);
			children.at(1).at(i) = parent_A.at(i);
		}

	}

	return children;
}

template<typename DNA>
inline void GeneticAlgorithm<DNA>::mutate(std::vector<DNA>& gene)
{
	size_t avg_mutations = std::ceil(gene.size() * mutationRate);
	distribution.param(std::uniform_int_distribution<int>::param_type(0, std::ceil(gene.size() * 4)));
	size_t index;
	for (size_t i = 0; i < avg_mutations*4; i++) // roll N times
	{
		index = distribution(generator);
		if (index < gene.size())
		{
			mutator(gene.at(index));
		}
	}
}

template<typename DNA>
inline void GeneticAlgorithm<DNA>::mutate_v2(std::vector<DNA>& gene)
{
    std::uniform_real_distribution<double> mutDist(0, 1);
    for (size_t i = 0; i < gene.size(); ++i)
    {
        if(mutDist(generator) < mutationRate)
        {
            mutator(gene.at(i));
        }
    }
}

template<typename DNA>
inline std::vector<size_t[2]> GeneticAlgorithm<DNA>::selectParents(const std::vector<double>& balot, size_t totalWins, size_t Ncouples)
{
	std::vector<size_t[2]> parents(Ncouples);
	int draw;
    distribution.param(std::uniform_int_distribution<int>::param_type(0, totalWins - 1));
	int acumWins;
	std::vector<double>::const_iterator iter;

	//std::cout << "Total wins:\t" << totalWins << std::endl;

	for (size_t i = 0; i < Ncouples; i++)
	{
		draw = distribution(generator);
		iter = balot.begin();
		acumWins = *iter;

		//std::cout << "Draw:\t" << draw << std::endl;

		while (acumWins <= draw)
		{
			iter++;
			if (iter == balot.end())
			{
				iter = balot.begin();
			}
			acumWins += *iter;
		}
		parents.at(i)[0] = std::distance(balot.begin(), iter) % population.size();
		//std::cout << "Winning number:\t" << acumWins << std::endl;
		//second parent:
		draw = distribution(generator);
		iter = balot.begin();
		acumWins = *iter;

		//std::cout << "Draw:\t" << draw << std::endl;
		while (acumWins <= draw || std::distance(balot.begin(), iter) % population.size() == parents.at(i)[0])
		{
			iter++;
			if (iter == balot.end())
			{
				iter = balot.begin();
			}
			acumWins += *iter;
		}
		parents.at(i)[1] = std::distance(balot.begin(), iter) % population.size();
		//std::cout << "Winning number:\t" << acumWins << std::endl
		//	<< "Winners:\t" << parents.at(i)[0] << "\t" << parents.at(i)[1] << std::endl;
		
	}
	
	return parents;
}

struct win_id
{
	win_id(double w, size_t id_) { wins = w;  id = id_; }
	double wins;
	size_t id;
};


template<typename DNA>
inline std::vector<size_t[2]> GeneticAlgorithm<DNA>::selectParents_v2(const std::vector<double>& wins, size_t Ncouples, size_t compare_sample_size)
{
	std::vector<win_id> balot;
	std::vector<size_t[2]> parents(Ncouples);
	for (size_t i = 0; i < wins.size(); i++)
	{
		balot.emplace_back(win_id(wins.at(i), i));
	}
	std::vector<win_id> candidates;
	for (size_t i = 0; i < Ncouples; i++)
	{
		candidates.clear();
        //std::__sample( balot.begin(), balot.end(), std::back_inserter(candidates), compare_sample_size, generator);
        std::sample(balot.begin(), balot.end(), std::back_inserter(candidates), compare_sample_size, generator);
		auto parent_A = std::max_element(candidates.begin(), candidates.end(), [](win_id A, win_id B)->bool {return A.wins < B.wins; });

		parent_A->wins = 0;

		auto parent_B = std::max_element(candidates.begin(), candidates.end(), [](win_id A, win_id B)->bool {return A.wins < B.wins; });
		
		parents.at(i)[0] = parent_A->id;
		parents.at(i)[1] = parent_B->id;
	}

	return parents;
}


