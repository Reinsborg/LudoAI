
#include "LudoAI.h"
#include <iostream>
#include <algorithm>

// GLOBE:  if(abs_square % 13 == 0 || abs_square % 13 == 8)

//bool inStart[16];
//bool inGoal[16];
//bool isSafe[16]; //on globe or two+ pieces together or goal stretch
//bool canBeKilled[4]; //in reach of enemy player
//bool canKill[4]; // can send enemy home with current dice roll
//bool beSafe[4]; // can get to safety (globe, teammate, goal stretch)
//bool canJump[4]; // can reach star with current dice roll
//bool canFinish[4]; // can reach goal pos with current dice roll
//bool willDie[4]; // will be sent home with current dice roll
//double progress[16];

/*decisionMaker = ANN(NPIECES * 4 + PLAYERPIECES * 7, PLAYERPIECES, 4, (NPIECES * 4 + PLAYERPIECES * 7) * 2);
decisionMaker.initialize(initializer);*/

//{
//	inStart = std::vector<bool>(NPIECES, true);
//	inGoal[16];
//	isSafe[16]; //on globe or two+ pieces together or goal stretch
//	canBeKilled[4]; //in reach of enemy player
//	canKill[4]; // can send enemy home with current dice roll
//	beSafe[4]; // can get to safety (globe, teammate, goal stretch)
//	canJump[4]; // can reach star with current dice roll
//	canFinish[4]; // can reach goal pos with current dice roll
//	willDie[4]; // will be sent home with current dice roll
//	isSix;
//	progress[16];
//}


std::vector<double> LudoAI::encodeGenome()
{
	return decisionMaker.encodeGenome();
}

void LudoAI::decodeGenome(const std::vector<double>& gene)
{
	decisionMaker.decodeGenome(gene);
}

LudoAI::LudoAI(ANN neuralNet) : inStart(NPIECES, true), inGoal(NPIECES, false), isSafe(NPIECES, true), canBeKilled(PLAYERPIECES, false),
canKill(PLAYERPIECES, false), beSafe(PLAYERPIECES, false), canJump(PLAYERPIECES, false),
canFinish(PLAYERPIECES, false), willDie(PLAYERPIECES, false), progress(NPIECES, 0.0), isValid(PLAYERPIECES, false)
{
	decisionMaker = neuralNet;

}


int LudoAI::make_decision()
{
	update_state();

	//std::cout << "Pos:\t\t" << std::vector<int>(position, &position[15]) << std::endl;
	//printState();
    std::vector<double> state = construct_state_vector();
    //std::transform(state.begin(), state.end(),state.begin(), [](double x)->double{return x*2 -1;});
    std::vector<double> prob = decisionMaker.forward(state);
	int move = std::max_element(prob.begin(), prob.end()) - prob.begin();
	//std::cout << move << std::endl;
	return move;
}


std::vector<double> LudoAI::construct_state_vector()
{
	std::vector<double> state;
	std::copy(inStart.begin(), inStart.end(), std::back_inserter(state));
	std::copy(inGoal.begin(), inGoal.end(), std::back_inserter(state));
	std::copy(isSafe.begin(), isSafe.end(), std::back_inserter(state));
	std::copy(canBeKilled.begin(), canBeKilled.end(), std::back_inserter(state));
	std::copy(canKill.begin(), canKill.end(), std::back_inserter(state));
	std::copy(beSafe.begin(), beSafe.end(), std::back_inserter(state));
	std::copy(canJump.begin(), canJump.end(), std::back_inserter(state));
	std::copy(canFinish.begin(), canFinish.end(), std::back_inserter(state));
	std::copy(willDie.begin(), willDie.end(), std::back_inserter(state));
	std::copy(isValid.begin(), isValid.end(), std::back_inserter(state));
	state.push_back(isSix);
	std::copy(progress.begin(), progress.end(), std::back_inserter(state));

	return state;
}


void LudoAI::printState()
{
	std::cout << "In start: \t" << inStart << std::endl
		<< "In goal: \t" << inGoal << std::endl
		<< "is safe:\t" << isSafe << std::endl
		<< "can be killed:\t" << canBeKilled << std::endl
		<< "can kill:\t" << canKill << std::endl
		<< "be safe:\t" << beSafe << std::endl
		<< "can jump:\t" << canJump << std::endl
		<< "can finish:\t" << canFinish << std::endl
		<< "will Die:\t" << willDie << std::endl
		<< "is six: \t" << isSix << std::endl
		<< "progress:\t" << progress << std::endl << std::endl << std::endl;
}

LudoAI::LudoAI() : inStart(NPIECES, true), inGoal(NPIECES, false), isSafe(NPIECES, true), canBeKilled(PLAYERPIECES, false),
canKill(PLAYERPIECES, false), beSafe(PLAYERPIECES, false), canJump(PLAYERPIECES, false),
canFinish(PLAYERPIECES, false), willDie(PLAYERPIECES, false), progress(NPIECES, 0.0), isValid(PLAYERPIECES, false), isSix(false)
{
}


void LudoAI::update_state()
{
	unsigned int color;
	unsigned int pos;
	isSix = dice == 6;

	// update states relavant to all pices: inStart, inGoal, isSafe
	// not all are inputs but helpers to calculate actual inputs
	for (size_t piece = 0; piece < NPIECES; piece++)
	{
		inStart[piece] = position[piece] < 0;
		inGoal[piece] = position[piece] == 99;
		if (piece < 4)
			isSafe[piece] = position[piece] % 13 == 0 || position[piece] % 13 == 8 || position[piece] >= 51 || position[piece] < 0;
		else
			isSafe[piece] = (position[piece] % 13 == 0 && position[piece] != 0) || position[piece] % 13 == 8 || position[piece] >= 51 || position[piece] < 0;
		color = piece / 4;
		if (!isSafe[piece]) // check if piece is protected by teammate
		{

			for (size_t teammate = color * 4; teammate < color * 4 + 4; teammate++)
			{
				if (teammate == piece)
					continue;
				else
				{
					if (position[teammate] == position[piece] && position[piece] != 0)
					{
						isSafe[piece] = true;
						//isSafe[teammate] = true;
					}
				}


			}
		}

		//calculate progress of each piece
		if (inStart[piece])
		{
			progress[piece] = 0;
		}
		else if (inGoal[piece])
		{
			progress[piece] = 1;
		}
		else
		{
			if (position[piece] > 55) // goal stretch is special case
			{
				pos = position[piece] - (5 * color);
			}
			else
			{
				pos = position[piece] - (13 * color);
				if (pos < 0)
				{
					pos += 52;
				}
			}
			progress[piece] = double(pos) / 56.0;
		}

	}

	// update states only applicable to friendly pieces ********************************************************************
	for (size_t friendly = 0; friendly < 4; friendly++)
	{
		canBeKilled[friendly] = false;
		canKill[friendly] = false;
		canFinish[friendly] = false;
		canJump[friendly] = false;
		beSafe[friendly] = false;
		willDie[friendly] = false;




		// if not safe, check if piece is in reach of enemy ****************************************************************
		if (!isSafe[friendly])
		{
			for (size_t enemy = 4; enemy < NPIECES; enemy++)
			{
				int dist = position[friendly] - position[enemy];
				if ((dist > 0 && dist <= 6) || (dist <= -46 && dist >= -51))
				{
					canBeKilled[friendly] = true;
				}
			}
		}
		//******************************************************************************************************************

		if (!is_valid_move(friendly))
		{
			isValid[friendly] = false;
		}
		else
		{
			isValid[friendly] = true;
			int new_pos;

			if (inStart[friendly])
			{
				new_pos = 0;
			}
			else
			{
				new_pos = position[friendly] + dice; // intermediate position of the piece after move
			}

			if (new_pos == 56) // check if goal, no furter action needed
			{
				canFinish[friendly] = true;
				beSafe[friendly] = true;
			}
			else
			{


				// check if it will die or kill at intermediate pos ****************************************************************
				int kill_die = kill_or_die(new_pos);
				if (kill_die == -1) // if the piece die at the intermediate pos no further investegation is needed
				{
					willDie[friendly] = true;
				}
				else
				{
					if (kill_die == 1)
					{
						canKill[friendly] = true;
					}

					// check if intermediate pos is a star and update arcordingly ***************************************************
					if (new_pos % 13 == 5 || new_pos == 50)
					{
						canJump[friendly] = true;
						new_pos += 6;
					}
					else if (new_pos % 13 == 11)
					{
						canJump[friendly] = true;
						new_pos += 7;
					}
					else
					{
						canJump[friendly] = false;
					}
					//***************************************************************************************************************


					if (new_pos == 56) // check if new position is the goal, no further action needed
					{
						canFinish[friendly] = true;
						beSafe[friendly] = true;
					}
					else
					{
						if (canJump[friendly]) // if we jumped recheck death/kill
						{
							int tmp = kill_or_die(new_pos);
							if (tmp == -1) // if the piece die at the intermediate pos no further investegation is needed
							{
								willDie[friendly] = true;
							}
							if (tmp == 1)
							{
								canKill[friendly] = true;
							}
						}
						beSafe[friendly] = new_pos % 13 == 8 || new_pos > 50; // land on globe or goal stretch
						if (!beSafe[friendly])
						{
							for (size_t teammate = 0; teammate < 4; teammate++) // land on teammate
							{
								if (teammate != friendly && position[teammate] == new_pos)
								{
									beSafe[friendly] = true;
								}
							}
						}
					}
				}
			}
		}
	}
}


int LudoAI::kill_or_die(int pos)
{
	for (size_t enemy = 4; enemy < NPIECES; enemy++)
	{

		if (position[enemy] == pos)
		{
			if (isSafe[enemy])
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}

	}
	return 0;
}

