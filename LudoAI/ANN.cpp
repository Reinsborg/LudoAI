#include "ANN.h"
// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <random>
#include <algorithm>
#include <iterator>
#include <functional>
#include <numeric>
#include <iostream>


// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

ANN::ANN()
{
}

ANN::ANN(size_t nInputs, size_t nOutputs, size_t nHLayers, size_t nNodes)
{
	inputDim.push_back(nInputs);
	for (size_t i = 0; i < nHLayers; i++)
	{
		inputDim.push_back(nNodes);
	}


	weights.resize(nHLayers + 1);
	biases.resize(nHLayers + 1);

	for (size_t layer = 0; layer < nHLayers; layer++)
	{
		weights.at(layer).resize(nNodes, std::vector<double>(inputDim.at(layer)));
		biases.at(layer).resize(nNodes);

	}
	weights.back().resize(nOutputs, std::vector<double>(inputDim.back()));
	biases.back().resize(nOutputs);

}

void ANN::setActivation(void (*actFunc)(const std::vector<double>& activity, std::vector<double>& output))
{
	for (size_t layer = 0; layer < weights.size(); layer++)
	{
		activation.push_back(actFunc);
	}
}

void ANN::setActivation(void (*actFunc)(const std::vector<double>&, std::vector<double>&), void (*outFunc)(const std::vector<double>&, std::vector<double>&))
{
	for (size_t layer = 0; layer < weights.size()-1; layer++)
	{
		activation.push_back(actFunc);
	}
	activation.push_back(outFunc);
}


void ANN::setActivation(std::vector< void (*)(const std::vector<double>& activity, std::vector<double>& output) > actFunctions)
{
	//assertm(actFunctions.size() == weights.size(), "Give one activation funtion for each layer");
	activation = actFunctions;

}

void ANN::initialize(std::function<double()>& initializer)
{
	for (size_t layer = 0; layer < weights.size(); layer++)
	{
		for (size_t outN = 0; outN < weights.at(layer).size(); outN++)
		{
			std::generate(weights.at(layer).at(outN).begin(), weights.at(layer).at(outN).end(), initializer);
			
		}
	}
	for (size_t layer = 0; layer < biases.size(); layer++)
	{
		std::generate(biases.at(layer).begin(), biases.at(layer).end(), initializer);
	}
}


std::vector<double> ANN::forward(const std::vector<double>& input)
{

	//if (input.size() != inputDim.at(0))
	//	throw "Input dimensions does not match";
	//else
	//	std::cout << "Input a OK" << std::endl;



	std::vector<double> activity(weights.front().size()); 
	std::vector<double> output = input;
	for (size_t layer = 0; layer < weights.size(); layer++)
	{
		activity.resize(weights.at(layer).size());

		std::vector<double>::const_iterator first_in = output.begin();
		std::vector<double>::const_iterator last_in = output.end();
		for (size_t outNode = 0; outNode < weights.at(layer).size(); outNode++)
		{
			activity.at(outNode) = std::inner_product(first_in, last_in, weights.at(layer).at(outNode).begin(), biases.at(layer).at(outNode));
		}

		activation.at(layer)(activity, output);
		
		
	}


	return output;
}



void RELU(const std::vector<double>& activity, std::vector<double>& output)
{
	output.resize(activity.size());
	for (size_t i = 0; i < activity.size(); i++)
	{
		output.at(i) = std::max(activity.at(i), 0.0);
	}

	
}

void sigmoid(const std::vector<double>& activity, std::vector<double>& output)
{
	output.resize(activity.size());
	for (size_t i = 0; i < activity.size(); i++)
	{
		output.at(i) = 1 / (1 + exp(-activity.at(i)));
	}

}

void tanh_vec(const std::vector<double>& activity, std::vector<double>& output)
{
	output.resize(activity.size());
	for (size_t i = 0; i < activity.size(); i++)
	{
		output.at(i) = std::tanh(activity.at(i));
	}
}

void softMax(const std::vector<double>& activity, std::vector<double>& output)
{
	output.resize(activity.size());
	double max_val = *std::max_element(activity.begin(), activity.end());
	double Z = 0.0;
	for (size_t i = 0; i < activity.size(); i++)
	{
		Z += exp(activity.at(i) - max_val);
	}
	for (size_t i = 0; i < activity.size(); i++)
	{
		output.at(i) = exp(activity.at(i) - max_val) / Z;
	}

}

std::vector< std::vector< std::vector< double >>>& ANN::getWeights()
{
	return weights;
}
std::vector< std::vector< double >>& ANN::getBiases()
{
	return biases;
}
void ANN::setWieghts(std::vector< std::vector< std::vector< double >>>& w)
{
	weights = w;
}
void ANN::setBiases(std::vector< std::vector< double >>& b)
{
	biases = b;
}

std::vector<double> ANN::encodeGenome()
{
	std::vector<double> gene;
	

	for (size_t layer = 0; layer < weights.size(); layer++)
	{
		for (size_t outNode = 0; outNode < weights.at(layer).size(); outNode++)
		{
			std::copy(weights.at(layer).at(outNode).begin(), weights.at(layer).at(outNode).end(), std::back_inserter(gene));
		}
	}
	for (size_t layer = 0; layer < biases.size(); layer++)
	{
		std::copy(biases.at(layer).begin(), biases.at(layer).end(), std::back_inserter(gene));
	}
	return gene;
}

void ANN::decodeGenome(const std::vector<double>& gene)
{
	std::vector<double>::const_iterator gene_iter = gene.begin();

	for (size_t layer = 0; layer < weights.size(); layer++)
	{
		for (size_t outNode = 0; outNode < weights.at(layer).size(); outNode++)
		{
			std::copy(gene_iter, gene_iter + weights.at(layer).at(outNode).size(), weights.at(layer).at(outNode).begin() );
			gene_iter = std::next(gene_iter, weights.at(layer).at(outNode).size());
		}
	}
	for (size_t layer = 0; layer < biases.size(); layer++)
	{
		std::copy(gene_iter, gene_iter + biases.at(layer).size(), biases.at(layer).begin());
		gene_iter = std::next(gene_iter, biases.at(layer).size());
	}

}