#pragma once
#include <iostream>
#include <algorithm>
#include "../Ludo-help-for-tools-of-AI/Domain-Code/iplayer.h"
#include "../Ludo-help-for-tools-of-AI/Domain-Code/positions_and_dice.h"
#include "io_vector.h"

#define NPIECES 16
#define PLAYERPIECES 4

class player_expert :
	public iplayer
{
public:
	player_expert();


	iplayer* clone() const { return new player_expert(*this); }
private:

	int make_decision();
	void update_state();
	int kill_or_die(int pos);


	std::vector<bool> inStart;
	std::vector<bool>  inGoal;
	std::vector<bool>  isSafe; //on globe or two+ pieces together or goal stretch
	std::vector<bool>  canBeKilled; //in reach of enemy player
	std::vector<bool>  canKill; // can send enemy home with current dice roll
	std::vector<bool>  beSafe; // can get to safety (globe, teammate, goal stretch)
	std::vector<bool>  canJump; // can reach star with current dice roll
	std::vector<bool>  canFinish; // can reach goal pos with current dice roll
	std::vector<bool>  willDie; // will be sent home with current dice roll
	std::vector<bool> isValid; // is able to move
	bool  isSix;
	std::vector<double>  progress;
};

